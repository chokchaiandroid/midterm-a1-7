

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class RamdomNumber {

    public static void main(String[] args) {

        Random rand = new Random();
        int n = 1400;
        int [] arr = new int[5*n];

        System.out.println("Random numbers between 1 - 5:");
        for (int i = 0; i < arr.length; i++) {
            arr[i] =  rand.nextInt(n) + 1;
            System.out.print(arr[i] + ", ");
        }

        try {
            Path file = Paths.get("inputtest.txt");
            BufferedWriter writer = Files.newBufferedWriter(file, 
                    StandardCharsets.UTF_8);   

            for (int i = 0; i < arr.length; i++) {
                writer.write((arr[i] + 1));
                writer.newLine();
            }

            writer.close();       
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }

        
    }
}
 